const Joi = require('joi');
const SchemaModel = require('../config/schema');

const config = require('../config/config');
const storeName = config.STORE_NAME;
const tableName = `${storeName}_orders`; //order table

const optionalParams = [
    'title',
    'paymentStatus',
    'deliveryAddress',
    'deliveryStatus',
    'cancellationStatus',
    'refundStatus',
    'replacementStatus',
    'orderDetails',
    'size',
    'subFrequency',
    'subUnit',
    'requestType',
    'price'
];
const attToGet = ['title', 'orderId', 'price','paymentStatus', 'deliveryStatus', 'deliveryAddress'];
const attToQuery = ['description', 'imageURL', 'price', 'size', 'title', 'collectionId'];

const orderSchema =  {
    hashKey: 'orderId',
    timestamps: true,
    schema: Joi.object({
        orderId: Joi.string().alphanum(),
        price: Joi.number().positive().min(1),
        title: Joi.string(),
        orderDetails: Joi.object(),
        paymentStatus: Joi.string(),
        cancellationStatus: Joi.boolean(),
        refundStatus: Joi.boolean(),
        replacementStatus: Joi.boolean(),
        deliveryAddress: Joi.string(),
        deliveryStatus: Joi.string(),
        size: Joi.string(),
        subFrequency: Joi.number().min(30),
        subUnit: Joi.string(),
        requestType: Joi.string()
    }).unknown(true).optionalKeys(optionalParams)
};

const optionsObj = {
    attToGet,
    attToQuery,
    optionalParams,
    tableName
};
const Order = SchemaModel(orderSchema, optionsObj);

module.exports = Order;